﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float vely;
    public float maxy;
    private float moveVertical;
    private Vector3 positionActual;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        moveVertical = Input.GetAxis("Vertical");

        positionActual = transform.position;

        if(moveVertical > 0)
        {
            positionActual.y = positionActual.y + vely * moveVertical;
        }
        if(positionActual.y >= maxy)
        {
            positionActual.y = maxy;
        }
        if(positionActual.y <= -maxy)
        {
            positionActual.y = -maxy;
        }
        else if(moveVertical < 0)
        {
            positionActual.y = positionActual.y + vely * moveVertical;
        }

        transform.position = positionActual;

    }
}
